<form method="post">
    <div>
        <input type="text" name="name" placeholder="Name"> <span><?=$name_error?></span>
    </div>
    <div>
        <input type="text" name="lastname" placeholder="Lastname"> <span><?=$lastname_error?></span>
    </div>
    <div>
        <input type="text" name="email" placeholder="e-mail"> <span><?=$email_error?></span>
    </div>
    <div>
        <label for="">Subjects:</label>
        <?php
            foreach($subjects as $_subject){
        ?>
        <div>
            <input type="checkbox" name="subject[]" value="<?=$_subject['subject']?>"> - <label for=""><?=$_subject['subject']." ( ".$_subject['ects']." ECTS)"?></label>
        </div>
        <?php
            } // Close ForeaEach
        ?>
    </div>
    <div>
        <input type="submit" name="student_submit" value="registration academically">
    </div>
</form>
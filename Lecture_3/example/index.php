<?php
    include "info_of_subjects.php";
    include "validate.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 3</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        if(isset($_POST['student_submit']) && 
            $name_error=="" && $lastname_error=="" && $email_error==""){
            include "manager_form.php";
        }else{
            include "students_form.php";
        }
    ?>
</body>
</html>
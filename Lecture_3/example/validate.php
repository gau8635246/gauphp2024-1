<?php
    $name_error = $lastname_error = $email_error =  "";
    
    if(isset($_POST['name']) && empty($_POST['name'])){
        $name_error = "Name is required";
    }

    if(isset($_POST['lastname']) && empty($_POST['lastname'])){
        $lastname_error = "Lastname is required";
    }

    if(isset($_POST['email']) && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $email_error= "Invalid Email";
    }
?>
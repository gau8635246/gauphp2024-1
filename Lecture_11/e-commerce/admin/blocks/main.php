<main>
    <nav>
        <ul>
            <li><a href="/">DeshBoard</a></li>
            <li><a href="?add">Add Category</a></li>
            <li><a href="?sign=out">Sign Out</a></li>
        </ul>
        <div class="info">
            <?php
                echo $_SESSION['name'];
                echo "<br>";
                echo $_SESSION['lastname'];
            ?>
        </div>
    </nav>
    <div class="content">
        <?php
            if(isset($_GET['add'])){
                include "add.php";
            }
        ?>
    </div>
</main>
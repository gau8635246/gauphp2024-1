<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 1</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Page 1</h1>
    <div>
        <a href="page2.php">P 2</a>
    </div>
    <div>
        <?php
            $p1 = "This is a Page 1";
            echo $p1;
            $_SESSION['s1'] = $p1." from session";
            echo "<hr>";
            echo $_SESSION['s1'];
            echo "<hr>";
            $_SESSION['s2'] = "Session s2";
            echo $_SESSION['s2'];
        ?>
    </div>
</body>
</html>
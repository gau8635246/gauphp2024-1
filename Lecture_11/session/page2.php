<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 2</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Page 2</h1>
    <div>
        <a href="page3.php">P 3</a>
    </div>
    <div>
        <?php
            echo $p1;
            echo "<hr>";
            echo $_SESSION['s1'];
            unset($_SESSION['s1']);
            // session_destroy();
            echo "<hr>";
            echo $_SESSION['s2'];
        ?>
    </div>
</body>
</html>
<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 3</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Page 3</h1>
    <div>
        <a href="page1.php">P 1</a>
    </div>
    <div>
        <?php
            echo $p1;
            echo "<hr>";
            echo $_SESSION['s1'];
            echo "<hr>";
            echo $_SESSION['s2'];
        ?>
    </div>
</body>
</html>
<?php
    session_start();
    include_once "config/connect.php";
    include "admin/blocks/signin.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>e-commerce</title>   
    <link rel="stylesheet" href="admin/style.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        Header
        <div class="sign">
            <div><a href="?sign=in">Sign In</a></div>
            <div><a href="">Sign Up</a></div>
        </div>
    </header>
    <?php
        include "blocks/nav.php";
    ?>
    <main>
        <?php 
            if(isset($_GET['sign']) && $_GET['sign']=="in"){
               include "admin/blocks/login_form.php"; 
            }
        ?>
    </main>
    <footer>Footer</footer>
</body>
</html>
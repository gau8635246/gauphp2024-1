<?php
    include_once "config/connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 9</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header></header>
    <?php
        include "blocks/nav.php";
    ?>
    <main></main>
    <footer></footer>
</body>
</html>